#!/usr/bin/env python3

import argparse
import datetime
import json
import os
import requests
import requests.exceptions
import string
import sys
import time
import unicodedata
from pathlib import Path

import spotipy
from spotipy.oauth2 import SpotifyOAuth

# Cache file paths (currently in $PWD)
FUNKWHALE_TRACKS_CACHE = Path('.') / '.funkwhale-tracks-cache'
SPOTIFY_TRACKS_CACHE = Path('.') / '.spotify-tracks-cache'

# Tokens and API URLs are given via environment variables
try:
    FUNKWHALE_TOKEN = os.environ['FUNKWHALE_TOKEN']
    FUNKWHALE_URL = os.environ['FUNKWHALE_URL']
except KeyError as e:
    print(f'Missing (at least) env variable {e}, please check your confiugration.', file=sys.stderr)
    sys.exit(1)

class FunkUA:
    'User-Agent helper class to query the Funkwhale API'

    def __init__(self):
        self.token = FUNKWHALE_TOKEN
        self.url = FUNKWHALE_URL.rstrip('/')

    def fetch(self, method: str, endpoint: str, params: dict = {}, body: dict = None) -> object:
        endpoint = endpoint.lstrip('/')

        kwargs = {}
        if body is not None:
            kwargs['json'] = body
        r = requests.request(
            method,
            f'{self.url}/api/v1/{endpoint}',
            headers={
                'Authorization': f'Bearer {self.token}',
                'Accept': 'application/json; encoding=utf-8'
            },
            params=params,
            **kwargs
        )

        # Handle rate limiting
        remaining_requests = r.headers['X-RateLimit-Remaining']
        if remaining_requests is not None and int(remaining_requests) == 0:
            retry_in = int(r.headers['X-ReateLimit-Remaining'])
            print(f'Rate limited! Funkwhale says to retry in {retry_in} seconds. Sleeping until then...', file=sys.stderr)
            time.sleep(retry_in)

        try:
            r.raise_for_status()
        except requests.exceptions.HTTPError as e:
            print(f'Request: {self.url}/api/v1/{endpoint}\nparams: {params}\nbody: {body}\nkwargs: {kwargs}\nresponse body: {r.content}')
            raise e
        return r.json()

    def all_pages(self, *args, **kwargs) -> list:
        kwargs.setdefault('params', {})
        all_results = []
        i = 1
        while True:
            kwargs['params'].update({
                'page_size': 10000,
                'page': i
            })

            results = self.fetch(*args, **kwargs)
            all_results.extend(results['results'])
            print(f'Got {len(results["results"])} results on page {i}. Total: {len(all_results)}/{results["count"]}.', file=sys.stderr)

            i += 1
            if not results['next']:
                break

        return all_results

    def tracks(self, no_cache: bool = False) -> list:
        if FUNKWHALE_TRACKS_CACHE.exists() and not no_cache:
            with FUNKWHALE_TRACKS_CACHE.open() as fp:
                return json.load(fp)

        print('Fetching tracks from funkwhale. This will be cached for subsequent runs.', file=sys.stderr)

        all_tracks = self.all_pages('get', 'tracks', params={'scope': 'me,subscribed'})

        with open(FUNKWHALE_TRACKS_CACHE, 'w') as fp:
            json.dump(all_tracks, fp)

        return all_tracks

def get_spotify_tracks(spotify: spotipy.Spotify, no_cache: bool) -> list:
    'Retreive the spotify tracks from spotify\'s API using spotipy'

    if SPOTIFY_TRACKS_CACHE.exists() and not no_cache:
        with SPOTIFY_TRACKS_CACHE.open() as fp:
            return json.load(fp)

    results = spotify.current_user_saved_tracks()
    spotify_tracks = results['items']
    while results['next']:
        results = spotify.next(results)
        spotify_tracks.extend(results['items'])
    print(f'Fetched {len(spotify_tracks)} tracks from spotify, caching.', file=sys.stderr)
    with SPOTIFY_TRACKS_CACHE.open('w') as fp:
        json.dump(spotify_tracks, fp)

    return spotify_tracks

def normalize_title(title: str) -> str:
    'Takes a track title, converts it to ASCII (if it is not in a foreign script), removes extra data (anything in parentheses or after a " - ").'

    # We want to first convert the title to ASCII. This converts (for instance) an em-dash into a
    # dash, or some weird stylizations into something more parseable.
    # However! This does not work if the title is not in a Latin script. In that casse we skip the
    # conversion step.

    title_bytes = title.encode('utf-8')
    not_ascii = [b for b in title_bytes if chr(b) not in string.printable]

    # If less than half of bytes are outside the ASCII range, carry on!
    if len(not_ascii) < len(title_bytes) / 2:
        title = unicodedata.normalize('NFKD', title)

    return title.split(' - ')[0].split('(')[0].lower().strip()

# https://stackoverflow.com/questions/8906926/formatting-timedelta-objects
def strfdelta(tdelta: datetime.timedelta, fmt: str) -> str:
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return fmt.format(**d)

def format_spotify_track(track: dict) -> str:
    artists = ', '.join([a['name'] for a in track['track']['artists']])
    album = track['track']['album']['name']
    title = track['track']['name']

    return f'\x1b[1m{artists} - {album} - {title}\x1b[0m'

def main():
    # Parse CLI arguments
    parser = argparse.ArgumentParser(
        description='Import tracks from Spotify into your Funkwhale instance.'
    )
    parser.add_argument(
        '--no-spotify-track-cache',
        help='Do not use the cached tracks from spotify (use if more tracks have been added on spotify since this script was last run)',
        action='store_true'
    )
    parser.add_argument(
        '--no-funkwhale-track-cache',
        help='Do not use the cached tracks from funkwhale (use if more tracks have been added on funkwhale since this script was last run)',
        action='store_true'
    )
    parser.add_argument(
        '--missing',
        help='Show missing tracks and exit',
        action='store_true'
    )
    parser.add_argument(
        '--missing-to-playlist',
        help='Store missing spotify tracks into this playlist (ID only, e.g. spotify:playlist:0000000000000000000000).'
    )
    args = parser.parse_args()

    if args.missing_to_playlist and not args.missing_to_playlist.startswith('spotify:playlist'):
        raise ValueError('Invalid spotify playlist. Give a spotify:playlist:0000000000000000000000.')


    # Get tracks from spotify and funkwhale, optionally cached from disk
    print('Fetching tracks from spotify. This will be cached on subsequent runs.', file=sys.stderr)
    spotify = spotipy.Spotify(client_credentials_manager=SpotifyOAuth(scope="user-library-read"))

    spotify_tracks = get_spotify_tracks(spotify, args.no_spotify_track_cache)

    funk = FunkUA()
    me = funk.fetch('get', '/users/me')
    print(f'Logged in as {me["username"]}', file=sys.stderr)

    funk_tracks = funk.tracks(args.no_funkwhale_track_cache)
    if not args.missing:
        funk_favorites = [t['track']['id'] for t in funk.all_pages('get', 'favorites/tracks')]
    else:
        funk_favorites = [] # No need to fetch favorites if we are just showing missing tracks

    print(f'Loaded {len(spotify_tracks)} spotify tracks, {len(funk_tracks)} funkwhale tracks, and {len(funk_favorites)} funkwhale favorites. Now going to match the former against the latter...', file=sys.stderr)



    # Establish a one-to-many correspondence between each spotify tracks and potential matching
    # funkwhale tracks.
    # We will ask the user to make a choice themselves later (or take the decision ourselves if we
    # are sure there won't be a duplicate).

    title_occurences = {} # Occurences of one track title (regardless of artists)
    links = [] # Links between a spotify track and and a list of matching funkwhale tracks

    for spotify_track in spotify_tracks:
        spotify_artists = [a['name'] for a in spotify_track['track']['artists']]
        spotify_artists_normalized = normalize_title(spotify_artists[0])
        spotify_title = spotify_track['track']['name']
        spotify_title_normalized = normalize_title(spotify_title)

        title_occurences.setdefault(spotify_title_normalized, [])
        title_occurences[spotify_title_normalized].append(spotify_track)

        track_links = {
            "from": spotify_track,
            "to": []
        }
        for track in funk_tracks:
            funk_title = track['title']
            funk_title_normalized = normalize_title(funk_title)
            if funk_title_normalized != spotify_title_normalized:
                continue

            funk_artist = track['artist']['name']
            if funk_artist not in spotify_artists and \
                normalize_title(funk_artist) != spotify_artists_normalized:
                continue

            track_links["to"].append(track)
        links.append(track_links)

    # Save missing tracks to playlist
    if args.missing_to_playlist:
        print(f'Saving missing tracks to {args.missing_to_playlist}')
        missing = [
            link['from']['track']['id']
            for link in links
            if not link['to']
        ]
        # Do it in chunks of 20 because spotipy doesn't break it down for us and spotify can't
        # handle many tracks at once
        for i in range(0, len(missing), 20):
            spotify.playlist_add_items(args.missing_to_playlist, missing[i:i + 20])


    # Show missing tracks/artists/albums
    # First we show every track without a correspondence, then a breakdown by artist/album
    if args.missing:
        artists = {}
        albums = {}

        for link in links:
            if link['to']:
                continue

            track = link["from"]
            track_artists = ', '.join([a['name'] for a in track['track']['artists']])
            track_album = track['track']['album']['name']
            album_artists = ', '.join([a['name'] for a in track['track']['album']['artists']])

            print(f'Missing: {format_spotify_track(track)}')

            artists.setdefault(track_artists, 0)
            artists[track_artists] += 1

            album_s = f'{album_artists} - {track_album}'
            albums.setdefault(album_s, 0)
            albums[album_s] += 1

        print('\n\nAlbums breakdown:\n')
        for album, count in sorted(albums.items(), key=lambda i: i[1], reverse=True):
            print(f'{album}: {count}')

        print('\n\nArtists breakdown:\n')
        for artist, count in sorted(artists.items(), key=lambda i: i[1], reverse=True):
            print(f'{artist}: {count}')

        return


    # For each one-to-many correspondence, decide what to do.
    # - If no match, we don't do anything;
    # - If one match, we add it to the favorites
    # - If several matches, we ask the user what to do (unless we have a good idea what's going on,
    #   see below)

    for link in links:
        matches = link['to']
        print(f'\n\n----------------\n{format_spotify_track(link["from"])} => Found {len(matches)} match{"es" if len(matches) != 1 else ""}.')
        if not matches:
            continue

        choice = 1
        if len(matches) > 1:
            faves = [
                match['id']
                for match in matches
                if match['id'] in funk_favorites
            ]

            # If there are as many tracks in our spotify library with this title (regardless of the
            # listed artist on spotify) as on funkwhale, then it is safe to assume these are the
            # same tracks and no action is needed.
            # Otherwise, the spotify library probably contains several versions of the same track,
            # and we can't assume that the one favorited in funkwhale is the same version.
            occurences = title_occurences[normalize_title(matches[0]['title'])]
            if len(faves) == len(occurences):
                print('Song already favorited, skipping...')
                continue
            elif faves:
                print('You also have the following versions of that same track in your spotify library:')
                for track in occurences:
                    if track['track']['id'] != link['from']['track']['id']:
                        print(f'\t{format_spotify_track(track)}')

            # Ask the user for their opinion
            # (Do not add the track, or choose from the list of tracks on funkwhale)
            # Return chooses the default value (the first one)
            print('\n0. Do not add')
            for i, match in enumerate(matches):
                i = i + 1
                if i == 1:
                    s = f'[{i}]'
                else:
                    s = str(i)

                duration = datetime.timedelta(seconds=match['uploads'][0]['duration'])
                duration = strfdelta(
                    duration,
                    '{minutes:02d}:{seconds:02d}' if duration < datetime.timedelta(hours=1) else
                    '{hours:02d}:{minutes:02d}:{seconds:02d}'
                )

                heart = ''
                if match['id'] in funk_favorites:
                    heart = '\u2661 '

                print(f'{s}. {heart}\x1b[1m{match["artist"]["name"]} - {match["album"]["title"]} - {match["title"]}\x1b[0m  [{duration}]')
            choice = None
            while choice is None:
                try:
                    c = input('> ') or 1
                    c = int(c)
                except ValueError:
                    print('Please enter a number!', file=sys.stderr)
                    continue
                if c >= 0 and c < len(matches) + 1:
                    choice = c
            if choice == 0:
                continue

        to_add = matches[choice - 1]

        # Finally, we favorite the track

        if to_add['id'] in funk_favorites:
            print('Skipping because already in favorites...')
        else:
            funk.fetch('post', 'favorites/tracks', body={'track': to_add['id']})

if __name__ == '__main__':
    main()
